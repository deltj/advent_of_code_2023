use std::io::BufRead;

#[derive(Clone, Debug)]
pub struct Schematic {
    pub numbers: Vec<SchematicNumber>,
    pub symbols: Vec<SchematicSymbol>,
}

#[derive(Clone, Debug)]
pub struct SchematicNumber {
    pub num: i32,
    pub x1: i32,
    pub x2: i32,
    pub y: i32,
}

impl SchematicNumber {
    pub fn new(num: i32, x1: i32, x2: i32, y: i32) -> Self {
        Self {
            num,
            x1,
            x2,
            y,
        }
    }
}

#[derive(Clone, Debug)]
pub struct SchematicSymbol {
    pub symbol: char,
    pub x: i32,
    pub y: i32,
}

impl SchematicSymbol {
    pub fn new(symbol: char, x: i32, y: i32) -> Self {
        Self {
            symbol,
            x,
            y,
        }
    }
}

/// Read the provided input into a Schematic object, including vectors of
/// numbers and symbols.  I decided to separate the number and symbol vectors
/// to make adjacency algorithms easier to implement and read.
pub fn read_engine_schematic(reader: &mut dyn BufRead) -> Schematic {
    let mut symbols: Vec<SchematicSymbol> = Vec::new();
    let mut numbers: Vec<SchematicNumber> = Vec::new();

    let mut x;
    let mut x1 = 0;
    let mut x2 = 0;
    let mut number_string: String = "".to_string();
    for (y, line_result) in reader.lines().enumerate() {
        x = 0;

        let line = line_result.unwrap();
        let line_trimmed = line.trim();
        
        for ch in line_trimmed.chars() {
            if ch == '.' {
                if !number_string.is_empty() {
                    //  This is the end of a schematic item
                    //println!("adding number {} with x1={}, x2={}, y={}", number_string, x1, x2, y);
                    numbers.push(SchematicNumber::new(number_string.parse::<i32>().unwrap(), x1, x2, y as i32));
                    number_string.clear();
                }
            } else if ch.is_ascii_digit() {
                if number_string.is_empty() {
                    //  This is the beginning of a new schematic item
                    x1 = x;
                }
                x2 = x;
                number_string.push(ch);
            } else {
                if !number_string.is_empty() {
                    //  This is the end of a schematic item
                    //println!("adding number {} with x1={}, x2={}, y={}", number_string, x1, x2, y);
                    numbers.push(SchematicNumber::new(number_string.parse::<i32>().unwrap(), x1, x2, y as i32));
                    number_string.clear();
                }

                //  This is a symbol
                symbols.push(SchematicSymbol::new(ch, x, y as i32));
            }
            x += 1;
        }

        //  Check for numbers at the end of a line
        if !number_string.is_empty() {
            //  This is the end of a schematic item
            //println!("adding number {} with x1={}, x2={}, y={}", number_string, x1, x2, y);
            numbers.push(SchematicNumber::new(number_string.parse::<i32>().unwrap(), x1, x2, y as i32));
            number_string.clear();
        }
        //y += 1;
    }

    Schematic { numbers, symbols }
}

/// Test whether the specified number has any adjacent symbols (if it does, it would
/// be a part number)
pub fn has_adjacent_symbol(num: &SchematicNumber, symbols: &Vec<SchematicSymbol>) -> bool {
    for symbol in symbols {
        if i32::abs(num.y - symbol.y) < 2 && symbol.x >= num.x1 - 1 && symbol.x <= num.x2 + 1 {
            return true;
        }
    }

    false
}

/// Find numbers adjacent to the specified symbol.  This is used for finding gears.
pub fn get_adjacent_numbers(gear: &SchematicSymbol, numbers: &Vec<SchematicNumber>) -> Vec<SchematicNumber> {
    let mut adj_nums: Vec<SchematicNumber> = Vec::new();

    for num in numbers {
        if i32::abs(num.y - gear.y) < 2 && gear.x >= num.x1 - 1 && gear.x <= num.x2 + 1 {
            adj_nums.push(num.clone());
        }
    }

    adj_nums
}

/// Find the sum of part numbers in the schematic
pub fn part_number_sum(schematic: &Schematic) -> i32 {
    let mut sum = 0;

    for num in &schematic.numbers {
        //  If the number has an adjacent symbol, then it's a part number
        if has_adjacent_symbol(num, &schematic.symbols) {
            sum += num.num;
        }
    }

    sum
}

/// Find the sum of gear ratios in the schematic
pub fn gear_ratio_sum(schematic: &Schematic) -> i32 {
    let mut sum = 0;

    for sym in &schematic.symbols {
        if sym.symbol == '*' {
            //  We're looking for *'s with exactly two adjacent numbers
            let adj_nums = get_adjacent_numbers(sym, &schematic.numbers);
            if adj_nums.len() == 2 {
                sum += adj_nums[0].num * adj_nums[1].num;
            }
        }
    }
    
    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn has_adjacent_symbol_test1() {
        let num = SchematicNumber::new(467, 0, 2, 0);
        let mut symbols: Vec<SchematicSymbol> = Vec::new();
        symbols.push(SchematicSymbol::new('*', 3, 1));

        assert!(has_adjacent_symbol(&num, &symbols));
    }

    #[test]
    fn has_adjacent_symbol_test2() {
        let num = SchematicNumber::new(633, 6, 8, 2);
        let mut symbols: Vec<SchematicSymbol> = Vec::new();
        symbols.push(SchematicSymbol::new('#', 6, 3));

        assert!(has_adjacent_symbol(&num, &symbols));
    }

    #[test]
    fn has_adjacent_symbol_test3() {
        let num = SchematicNumber::new(633, 6, 8, 2);
        let mut symbols: Vec<SchematicSymbol> = Vec::new();
        symbols.push(SchematicSymbol::new('*', 3, 1));

        assert!(!has_adjacent_symbol(&num, &symbols));
    }

    #[test]
    fn get_adjacent_numbers_test1() {
        let num1 = SchematicNumber::new(467, 0, 2, 0);
        let num2 = SchematicNumber::new(35, 2, 3, 2);
        let gear = SchematicSymbol::new('*', 3, 1);
        let mut numbers: Vec<SchematicNumber> = Vec::new();
        numbers.push(num1);
        numbers.push(num2);
        let adj_nums = get_adjacent_numbers(&gear, &numbers);
        assert_eq!(2, adj_nums.len());
    }
}