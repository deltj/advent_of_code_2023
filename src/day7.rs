use std::cmp::Ordering;
use std::io::BufRead;
use itertools::Itertools;

static mut USING_JOKERS: bool = false;

//  I'm sorry for using global state, but I can't think of a better way to 
//  pass this to compare_hands, which is called by cmp.
fn using_jokers() -> bool {
    unsafe {
        return USING_JOKERS;
    }
}

pub fn use_jokers(flag: bool) {
    unsafe {
        USING_JOKERS = flag;
    }
}

#[derive(Clone,Debug,Eq)]
pub struct Hand {
    pub cards: [i8; 5],
    pub bet: i32,
}

impl Hand {
    pub fn new(cards: &[i8; 5], bet: &i32) -> Self {
        Self {
            cards: *cards,
            bet: *bet,
        }
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        compare_hands(self, other)
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        self.cards == other.cards
    }
}

#[derive(Debug,PartialEq,PartialOrd)]
pub enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

//  Cards:    2, 3, 4, 5, 6, 7, 8, 9, T,  J,  Q,  K,  A
//  Values:   2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
pub fn char_to_ord(c: char) -> i8 {
    match c {
        'J' => {
            if using_jokers() {
                1
            } else {
                11
            }
        }
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
        'T' => 10,
        'Q' => 12,
        'K' => 13,
        'A' => 14,
        _ => 0
    }
}

pub fn parse_hand(hand_str: &str) -> Vec<i8> {
    let mut hand: Vec<i8> = Vec::new();
    for c in hand_str.chars() {
        hand.push(char_to_ord(c));
    }
    hand
}

pub fn analyze_hand(hand: &[i8]) -> HandType {
    if hand.len() != 5 {
        panic!("expected 5 cards");
    }
    //println!("hand: {:?}", hand);

    let mut unique_cards = hand.iter().cloned().unique().collect_vec();
    let mut card_counts = Vec::<usize>::new();
    for unique_card in unique_cards.iter() {
        if *unique_card == 1 {
            card_counts.push(0);
        } else {
            card_counts.push(hand.iter().filter(|&card| card == unique_card).count());
        }
    }

    if using_jokers() {
        //  Determine card with the highest frequency
        let max_freq_index = card_counts.iter().enumerate().max_by(|(_, a), (_, b)| a.cmp(b)).map(|(index, _)| index).unwrap();
        //println!("max_freq_index: {}", max_freq_index);
        let max_freq_card = unique_cards[max_freq_index];
        //println!("max_freq_card: {}", max_freq_card);

        //  Replace Jokers with this card
        let mut new_hand: Vec<i8> = Vec::new();
        for card in hand {
            if *card == 1 {
                new_hand.push(max_freq_card);
            } else {
                new_hand.push(*card);
            }
        }
        //println!("new hand: {:?}", new_hand);

        //  Re-analyze card counts
        unique_cards = new_hand.iter().cloned().unique().collect_vec();
        card_counts.clear();
        for unique_card in unique_cards.iter() {
            card_counts.push(new_hand.iter().filter(|&card| card == unique_card).count());
        }
        //println!("updated unique cards: {:?}", unique_cards);
        //println!("updated card counts: {:?}", card_counts);
    }

    //  Detect five of a kind
    if unique_cards.len() == 1 {
        return HandType::FiveOfAKind;
    }

    //  Detect four of a kind
    if card_counts.contains(&4) {
        return HandType::FourOfAKind;
    }

    //  Detect full house
    if unique_cards.len() == 2 {
        return HandType::FullHouse;
    }

    //  Detect three of a kind
    if card_counts.contains(&3) {
        return HandType::ThreeOfAKind;
    }

    //  Detect two pair
    if card_counts.len() == 3 && card_counts.contains(&2) && card_counts.contains(&1) {
        return HandType::TwoPair;
    }

    //  Detect one pair
    if card_counts.len() == 4 && card_counts.contains(&2) {
        return HandType::OnePair;
    }

    //  Detect high card
    if unique_cards.len() == 5 {
        return HandType::HighCard;
    }

    panic!("unrecognized hand type");
}

pub fn compare_hands(a: &Hand, b: &Hand) -> Ordering {
    let a_type = analyze_hand(&a.cards);
    let b_type = analyze_hand(&b.cards);
    if a_type > b_type {
        return Ordering::Greater;
    } else if a_type < b_type {
        return Ordering::Less;
    } else {
        for i in 0..5 {
            if a.cards[i] > b.cards[i] {
                return Ordering::Greater;
            } else if a.cards[i] < b.cards[i] {
                return Ordering::Less;
            }
        }
    }
    Ordering::Equal
}

pub fn total_winnings(hands: &Vec<Hand>) -> i32 {
    let mut tmp_hands: Vec<Hand> = hands.clone();
    tmp_hands.sort();
    let mut total_winnings = 0;
    for (i, hand) in tmp_hands.iter().enumerate() {
        let rank = (i + 1) as i32;
        total_winnings += hand.bet * rank;
    }
    total_winnings

}

pub fn read_hands(reader: &mut dyn BufRead) -> Vec<Hand> {
    let mut hands: Vec<Hand> = Vec::new();
    for line_result in reader.lines() {
        let line = line_result.unwrap();
        //println!("processing line {}", line);
        let tokens: Vec<&str>  = line.split_whitespace().collect();
        let cards = parse_hand(tokens[0]);
        let bet = tokens[1].parse::<i32>().unwrap();
        let hand = Hand::new(&cards.try_into().unwrap(), &bet);
        hands.push(hand);
    }
    hands
}

#[cfg(test)]
mod tests {
    use super::*;
    use serial_test::serial;

    #[test]
    #[serial]
    fn parse_hand_test() {
        use_jokers(false);
        let hand_str = "2345J";
        let hand = parse_hand(hand_str);
        assert_eq!(5, hand.len());
    }

    #[test]
    #[serial]
    fn parse_hand_joker_test() {
        use_jokers(true);
        let hand_str = "2345J";
        let hand = parse_hand(hand_str);
        assert_eq!(5, hand.len());
    }

    #[test]
    #[serial]
    fn analyze_hand_test_fiveofakind() {
        use_jokers(false);
        let hand_str = "AAAAA";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::FiveOfAKind, hand_type);
    }

    #[test]
    #[serial]
    fn analyze_hand_test_fourofakind() {
        use_jokers(false);
        let hand_str = "AA8AA";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::FourOfAKind, hand_type);
    }

    #[test]
    #[serial]
    fn analyze_hand_test_fullhouse() {
        use_jokers(false);
        let hand_str = "23332";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::FullHouse, hand_type);
    }

    #[test]
    #[serial]
    fn analyze_hand_test_threeofakind() {
        use_jokers(false);
        let hand_str = "QQQJA";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::ThreeOfAKind, hand_type);
    }

    #[test]
    #[serial]
    fn analyze_hand_test_threeofakind_jokers() {
        use_jokers(true);
        let hand_str = "QQQJA";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::FourOfAKind, hand_type);
    }

    #[test]
    #[serial]
    fn analyze_hand_test_twopair() {
        use_jokers(false);
        let hand_str = "23432";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::TwoPair, hand_type);
    }

    #[test]
    #[serial]
    fn analyze_hand_test_onepair() {
        use_jokers(false);
        let hand_str = "32T3k";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::OnePair, hand_type);
    }

    #[test]
    #[serial]
    fn analyze_hand_test_highcard() {
        use_jokers(false);
        let hand_str = "23456";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::HighCard, hand_type);
    }

    #[test]
    #[serial]
    fn compare_hands_test() {
        use_jokers(false);
        let hand_str1 = "2AAAA";
        let hand1 = parse_hand(hand_str1);
        let h1 = Hand::new(&hand1.try_into().unwrap(), &0);

        let hand_str2 = "1AAAA";
        let hand2 = parse_hand(hand_str2);
        let h2 = Hand::new(&hand2.try_into().unwrap(), &0);

        assert_eq!(Ordering::Greater, compare_hands(&h1, &h2));
    }

    #[test]
    #[serial]
    fn hands_sort_test() {
        use_jokers(false);
        let hand_str1 = "2AAAA";
        let hand1 = parse_hand(hand_str1);
        let h1 = Hand::new(&hand1.try_into().unwrap(), &1);

        let hand_str2 = "1AAAA";
        let hand2 = parse_hand(hand_str2);
        let h2 = Hand::new(&hand2.try_into().unwrap(), &0);

        let mut hands = Vec::<Hand>::new();
        hands.push(h1);
        hands.push(h2);
        hands.sort();
        assert_eq!(0, hands[0].bet);
        assert_eq!(1, hands[1].bet);
    }

    #[test]
    #[serial]
    fn example_joker_test() {
        use_jokers(true);
        let hand_str = "T55J5";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::FourOfAKind, hand_type);
    }

    #[test]
    #[serial]
    fn example_joker_test2() {
        use_jokers(true);
        let hand_str = "KTJJT";
        let hand = parse_hand(hand_str);
        let hand_type = analyze_hand(&hand);
        assert_eq!(HandType::FourOfAKind, hand_type);
    }

}