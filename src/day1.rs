use std::io::BufRead;
use std::cmp::min;

const NUMBER_WORDS: &[&str] = &[
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
];

const NUMBER_CHARS: [char; 10] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

/// Given a string containing some letters and numbers, some of which may be
/// actual numbers or number words, return a string containing only the digit 
/// representation of found numbers and number words
pub fn replace_number_words(line: &str) -> String {
    let mut result = "".to_string();

    for i in 0..line.len() {
        //  Look for digits
        let c = line.chars().nth(i).unwrap();
        if c.is_ascii_digit() {
            result.push(c);
        } else {
            //  Look for number words
            let slice_len = min(5, line.len() - i);
            let slice = &line[i..i + slice_len];
            for j in 0..10 {
                if slice.starts_with(NUMBER_WORDS[j]) {
                    //  Append the digit for this number word to the new string
                    result.push(NUMBER_CHARS[j]);
                    break;
                }
            }
        }
    }

    result
}

/// Read the calibration number for this line (which is the number
/// found by combining the first and last digits in the line)
/// 
/// If rep is true, replace number words with digits before processing
fn read_cal_line(line: &str, rep: bool) -> u32 {
    let actual_string = if rep { replace_number_words(line) } else { line.to_string() };

    let mut dig_num = 0;
    let mut f_dig: u32 = 0;
    let mut l_dig: u32 = 0;
    for ch in actual_string.chars() {
        if ch.is_ascii_digit() {
            let digit = ch.to_digit(10).unwrap();

            if dig_num == 0 {
                f_dig = digit;
                l_dig = digit;
            } else {
                    l_dig = digit;
            }

            dig_num += 1;
        }
    }

    f_dig * 10 + l_dig
}

pub fn read_cal_vector(reader: &mut dyn BufRead, rep: bool) -> Vec<u32> {
    let mut cal_vector: Vec<u32> = Vec::new();

    for line_result in reader.lines() {

        let line = line_result.unwrap();
        let line_trimmed = line.trim();
        let cal = read_cal_line(line_trimmed, rep);
        cal_vector.push(cal);
    }

    cal_vector
}

pub fn cal_sum(cal_vec: &[u32]) -> u32 {
    cal_vec.iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn replace_number_words_test() {
        assert_eq!("123", replace_number_words("onetwothree"));
        assert_eq!("123", replace_number_words("abconetwothree"));
        assert_eq!("82", replace_number_words("eightwo"));
        assert_eq!("18", replace_number_words("oneight"));
        assert_eq!("2", replace_number_words("2h"));
        assert_eq!("285", replace_number_words("2phhz85"));
        assert_eq!("9", replace_number_words("phblxtj9"));
    }

    #[test]
    fn read_cal_line_test() {
        assert_eq!(22, read_cal_line("2h", true));
        assert_eq!(25, read_cal_line("2phhz85", true));
        assert_eq!(99, read_cal_line("phblxtj9", true));
        assert_eq!(93, read_cal_line("four93kssrxr", false));
        assert_eq!(43, read_cal_line("four93kssrxr", true));
        assert_eq!(96, read_cal_line("9h6nine", false));
        assert_eq!(99, read_cal_line("9h6nine", true));
        assert_eq!(85, read_cal_line("vpeightwo15", true));
        assert_eq!(29, read_cal_line("2threefourfournine", true));
    }
}