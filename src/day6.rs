use std::io::BufRead;

pub struct Race {
    pub duration: i64,
    pub max_distance: i64,
}

impl Race {
    pub fn new(duration: i64, max_distance: i64) -> Self {
        Self {
            duration,
            max_distance,
        }
    }
}

pub fn read_races(reader: &mut dyn BufRead) -> Vec<Race> {
    let mut races: Vec<Race> = Vec::new();
    for line_result in reader.lines() {
        let line = line_result.unwrap();
        let tokens: Vec<&str>  = line.split_whitespace().collect();
        if tokens[0].contains("Time") {
            for token in &tokens[1..] {
                let dur = token.parse::<i64>().unwrap();
                races.push(Race::new(dur, 0));
            }
        } else if tokens[0].contains("Distance") {
            for i in 1..tokens.len() {
                let dist = tokens[i].parse::<i64>().unwrap();
                races[i - 1].max_distance = dist;
            }
        }
    }
    races
}

pub fn read_race(reader: &mut dyn BufRead) -> Vec<Race> {
    let mut time_str: String = "".to_string();
    let mut dist_str: String = "".to_string();
    for line_result in reader.lines() {
        let line = line_result.unwrap();
        let tokens: Vec<&str>  = line.split_whitespace().collect();
        if tokens[0].contains("Time") {
            for token in &tokens[1..] {
                time_str += token;
            }
        } else if tokens[0].contains("Distance") {
            for token in &tokens[1..] {
                dist_str += token;
            }
        }
    }
    //println!("time_str={}, dist_str={}", time_str, dist_str);
    let time = time_str.parse::<i64>().unwrap();
    let dist = dist_str.parse::<i64>().unwrap();
    let race = Race::new(time, dist);
    let mut races: Vec<Race> = Vec::new();
    races.push(race);
    return races;
}

pub fn charge_time_bounds(race: &Race) -> (i64, i64) {
    // maximize distance
    // dist (d) = speed (v) * time (t)
    // speed (v) = charge_duration (c)
    // time (t) = race_duration (r) - charge_duration (c)
    // d = c * (r - c)
    // d = c*r - c*c
    // d = c*r - c^2
    // c^2 - r*c - d = 0
    // (-b +- sqrt(b^2 - 4ac)) / 2a

    // lower bound = (r - sqrt(r^2 - 4*1*d)) / 2*1
    //             = (r - sqrt(r^2 - 4d)) / 2
    // upper bound = (r + sqrt(r^2 - 4d)) / 2
    let r = race.duration as f64;
    let d = (race.max_distance + 1) as f64;
    let lb = (r - (r * r - 4.0 * d).sqrt()) / 2.0;
    let ub = (r + (r * r - 4.0 * d).sqrt()) / 2.0;
    //println!("lb={}, b2={}", lb, ub);
    (lb.ceil() as i64, ub.floor() as i64)
}

pub fn winning_duration_product(races: &Vec<Race>) -> i64 {
    let mut result = 1;
    for race in races {
        let (lb, ub) = charge_time_bounds(race);
        result *= ub - lb + 1;
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn charge_time_bounds_test() {
        let race = Race::new(7, 9);
        let (b1, b2) = charge_time_bounds(&race);
        assert_eq!(2, b1);
        assert_eq!(5, b2);

        let race = Race::new(15, 40);
        let (b1, b2) = charge_time_bounds(&race);
        assert_eq!(4, b1);
        assert_eq!(11, b2);

        let race = Race::new(30, 200);
        let (b1, b2) = charge_time_bounds(&race);
        assert_eq!(11, b1);
        assert_eq!(19, b2);
    }
}