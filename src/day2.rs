use std::io::BufRead;

#[derive(Clone, Debug)]
pub struct CubeGameRound {
    pub red: u32,
    pub green: u32,
    pub blue: u32,
}

impl CubeGameRound {
    pub fn new(red: u32, green: u32, blue: u32) -> Self {
        Self {
            red,
            green,
            blue,
        }
    }
}

#[derive(Clone, Debug)]
pub struct CubeGame {
    pub id: u32,
    pub rounds: Vec<CubeGameRound>,
}

impl CubeGame {
    pub fn new(id: u32) -> Self {
        Self {
            id,
            rounds: Vec::new(),
        }
    }
}

/// Read a single line of input into a CubeGame struct
fn read_game_line(line: &str) -> CubeGame {
    let line_tokens: Vec<&str> = line.split(':').collect();

    //  Read game ID
    let game_id_tokens: Vec<&str> = line_tokens[0].trim().split(' ').collect();
    let game_id = game_id_tokens[1].parse::<u32>().unwrap();
    let mut game = CubeGame::new(game_id);

    //  Read game rounds
    let game_round_tokens: Vec<&str> = line_tokens[1].trim().split(';').collect();
    for game_round_str in game_round_tokens {
        let mut red: u32 = 0;
        let mut green: u32 = 0;
        let mut blue: u32 = 0;

        let cube_tokens: Vec<&str> = game_round_str.trim().split(',').collect();
        for cube_str in cube_tokens {
            let cube_count_tokens: Vec<&str> = cube_str.trim().split(' ').collect();
            let count = cube_count_tokens[0].parse::<u32>().unwrap();
            let color = cube_count_tokens[1];

            match color {
                "red" => { red = count },
                "green" => { green = count },
                "blue" => { blue = count },
                _ => { panic!("unrecognized color: {}", color) },
            }
        }

        let round = CubeGameRound::new(red, green, blue);
        game.rounds.push(round);
    }

    game
}

/// Find the minimum number of cubes required for the specified game
pub fn min_cubes(game: &CubeGame) -> CubeGameRound {
    let mut min_red = 0;
    let mut min_green = 0;
    let mut min_blue = 0;
    for round in &game.rounds {
        if round.red > min_red { min_red = round.red; }
        if round.green > min_green { min_green = round.green; }
        if round.blue > min_blue { min_blue = round.blue; }
    }

    CubeGameRound::new(min_red, min_green, min_blue)
}

/// Find total cube power for the specified set of games
pub fn total_cube_power(games: &Vec<CubeGame>) -> u32 {
    let mut total_power: u32 = 0;

    for game in games {
        let mg = min_cubes(game);
        let power = mg.red * mg.green * mg.blue;
        total_power += power;
    }

    total_power
}

/// Validate the specified game by checking whether it would be possible given
/// the specified cube count maximums
pub fn validate_game(game: &CubeGame, max_red: u32, max_green: u32, max_blue: u32) -> bool {
    for round in &game.rounds {
        if round.red > max_red { return false; }
        if round.green > max_green { return false; }
        if round.blue > max_blue { return false; }
    }

    true
}

/// Find the sum of valid game IDs in the specified set of games
pub fn sum_of_valid_game_ids(games: &Vec<CubeGame>, max_red: u32, max_green: u32, max_blue: u32) -> u32 {
    let mut valid_game_id_sum: u32 = 0;

    for game in games {
        if validate_game(game, max_red, max_green, max_blue) {
            valid_game_id_sum += game.id;
        }
    }

    valid_game_id_sum
}

/// Read lines of input into a vector of CubeGames
pub fn read_game_vector(reader: &mut dyn BufRead) -> Vec<CubeGame> {
    let mut game_vector: Vec<CubeGame> = Vec::new();

    for line_result in reader.lines() {

        let line = line_result.unwrap();
        let line_trimmed = line.trim();
        let game = read_game_line(line_trimmed);
        game_vector.push(game);
    }

    game_vector
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_game_line_test() {
        let game = read_game_line("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green");
        assert_eq!(1, game.id);
        assert_eq!(3, game.rounds.len());
        assert_eq!(4, game.rounds[0].red);
        assert_eq!(0, game.rounds[0].green);
        assert_eq!(3, game.rounds[0].blue);
        assert_eq!(1, game.rounds[1].red);
        assert_eq!(2, game.rounds[1].green);
        assert_eq!(6, game.rounds[1].blue);
        assert_eq!(0, game.rounds[2].red);
        assert_eq!(2, game.rounds[2].green);
        assert_eq!(0, game.rounds[2].blue);
    }

    #[test]
    fn validate_game_test() {
        let game1 = read_game_line("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green");
        assert!(validate_game(&game1, 12, 13, 14));

        let game3 = read_game_line("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red");
        assert!(!validate_game(&game3, 12, 13, 14))
    }

    #[test]
    fn min_cubes_test() {
        let game1 = read_game_line("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green");
        let m1 = min_cubes(&game1);
        assert_eq!(4, m1.red);
        assert_eq!(2, m1.green);
        assert_eq!(6, m1.blue);

        let game3 = read_game_line("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red");
        let m2 = min_cubes(&game3);
        assert_eq!(20, m2.red);
        assert_eq!(13, m2.green);
        assert_eq!(6, m2.blue);
    }
}