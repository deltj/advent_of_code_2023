use std::io::BufRead;

pub enum InputParserState {
    Seeds,
    Map,
}

#[derive(Debug)]
pub struct RangeMap {
    pub dst_start: u64,
    pub src_start: u64,
    pub count: u64,
}

impl RangeMap {
    pub fn new(dst_start: u64, src_start: u64, count: u64) -> Self {
        Self {
            dst_start,
            src_start,
            count,
        }
    }
}

pub struct SeedRange {
    pub start: u64,
    pub count: u64,
}

impl SeedRange {
    pub fn new(start: u64, count: u64) -> Self {
        Self {
            start,
            count,
        }
    }
}

pub fn seed_range_includes(seed: u64, seeds: &Vec<SeedRange>) -> bool {
    for sr in seeds {
        if seed >= sr.start && seed <= sr.start + sr.count {
            return true;
        }
    }
    false
}

pub fn map_includes_src(src: u64, map: &RangeMap) -> bool {
    src >= map.src_start && src <= map.src_start + map.count - 1
}

pub fn map_includes_dst(dst: u64, map: &RangeMap) -> bool {
    dst >= map.dst_start && dst <= map.dst_start + map.count - 1
}

pub fn get_map_dst(src: u64, map: &RangeMap) -> u64 {
    let offset = src - map.src_start;
    if offset > map.count - 1 {
        panic!("src outside map range");
    }
    map.dst_start + offset
}

pub fn get_map_src(dst: u64, map: &RangeMap) -> u64 {
    let offset = dst - map.dst_start;
    if offset > map.count - 1 {
        panic!("dst outside map range");
    }
    map.src_start + offset
}

pub fn read_maps(reader: &mut dyn BufRead, seed_range: bool) -> (Vec<SeedRange>, Vec<Vec<RangeMap>>) {
    let mut parser_state: InputParserState = InputParserState::Seeds;

    let mut seeds: Vec<SeedRange> = Vec::new();
    let mut maps: Vec<Vec<RangeMap>> = Vec::new();
    let mut i = 0;
    for line_result in reader.lines() {
        let line = line_result.unwrap();
        let line_trimmed = line.trim();

        match parser_state {
            InputParserState::Seeds => {
                if line_trimmed.contains("seeds") {
                    let seed_tokens: Vec<&str>  = line_trimmed.split_whitespace().collect();
                    if seed_range {
                        for i in 0..(seed_tokens.len()-1)/2 {
                            let start = seed_tokens[i*2+1].parse::<u64>().unwrap();
                            let count = seed_tokens[i*2+2].parse::<u64>().unwrap();
                            let sr = SeedRange::new(start, count);
                            seeds.push(sr);
                        }
                    } else {
                        for i in 1..seed_tokens.len() {
                            let start = seed_tokens[i].parse::<u64>().unwrap();
                            let count = 1;
                            let sr = SeedRange::new(start, count);
                            seeds.push(sr);
                        }
                    }
                } else if line_trimmed.is_empty() {
                    parser_state = InputParserState::Map;
                }
            },
            InputParserState::Map => {
                if line_trimmed.contains("map") {
                    //  Add the ith map
                    maps.push(Vec::new());
                } else if line.is_empty() {
                    i += 1;
                } else {
                    let map_tokens: Vec<&str> = line_trimmed.split_whitespace().collect();
                    let dst_start = map_tokens[0].parse::<u64>().unwrap();
                    let src_start = map_tokens[1].parse::<u64>().unwrap();
                    let count = map_tokens[2].parse::<u64>().unwrap();
                    maps[i].push(RangeMap::new(dst_start, src_start, count));
                }
            }
        }
    }

    (seeds, maps)
}

pub fn seed_to_location(seed: u64, maps: &Vec<Vec<RangeMap>>) -> u64 {
    let mut result = seed;
    for i in 0..maps.len() {
        for j in 0..maps[i].len() {
            if map_includes_src(result, &maps[i][j]) {
                result = get_map_dst(result, &maps[i][j]);
                break;
            }
        }
    }
    result
}

pub fn location_to_seed(loc: u64, maps: &Vec<Vec<RangeMap>>) -> u64 {
    let mut result = loc;
    for i in (0..maps.len()).rev() {
        for j in (0..maps[i].len()).rev() {
            if map_includes_dst(result, &maps[i][j]) {
                result = get_map_src(result, &maps[i][j]);
                break;
            }
        }
    }
    result
}

pub fn map_seeds(seeds: &Vec<u64>, maps: &Vec<Vec<RangeMap>>) -> Vec<u64> {
    let mut locs: Vec<u64> = Vec::new();
    for seed in seeds {
        let loc = seed_to_location(*seed, maps);
        locs.push(loc);
    }
    locs
}

pub fn find_lowest_loc_bf(seeds: &Vec<SeedRange>, maps: &Vec<Vec<RangeMap>>) -> u64 {
    for loc in 0..u64::MAX {
        let seed = location_to_seed(loc, maps);
        if seed_range_includes(seed, &seeds) {
            return loc;
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn map_includes_src_test() {
        let map = RangeMap::new(50, 98, 2);
        assert!(!map_includes_src(97, &map));
        assert!(map_includes_src(98, &map));
        assert!(map_includes_src(99, &map));
        assert!(!map_includes_src(100, &map));
    }

    #[test]
    fn map_includes_dst_test() {
        let map = RangeMap::new(50, 98, 2);
        assert!(!map_includes_dst(49, &map));
        assert!(map_includes_dst(50, &map));
        assert!(map_includes_dst(51, &map));
        assert!(!map_includes_dst(52, &map));
    }

    #[test]
    fn get_map_dst_test() {
        let map = RangeMap::new(50, 98, 2);
        assert_eq!(50, get_map_dst(98, &map));
        assert_eq!(51, get_map_dst(99, &map));
    }

    #[test]
    fn get_map_src_test() {
        let map = RangeMap::new(50, 98, 2);
        assert_eq!(98, get_map_src(50, &map));
        assert_eq!(99, get_map_src(51, &map));
    }
}