use std::io::BufRead;

#[derive(Clone, Debug)]
pub struct ScratchCard {
    pub card_num: u32,
    pub winning_numbers: Vec<i32>,
    pub card_numbers: Vec<i32>,
}

impl ScratchCard {
    pub fn new(card_num: u32) -> Self {
        Self {
            card_num,
            winning_numbers: Vec::new(),
            card_numbers: Vec::new(),
        }
    }
}

pub fn read_card_line(line: &str) -> ScratchCard {
    let line_tokens: Vec<&str> = line.trim().split(':').collect();
    let card_tokens: Vec<&str> = line_tokens[0].split_whitespace().collect();
    let card_num = card_tokens[1].parse::<u32>().unwrap();
    let number_tokens: Vec<&str> = line_tokens[1].trim().split('|').collect();
    let winning_number_tokens: Vec<&str> = number_tokens[0].split_whitespace().collect();
    let card_number_tokens: Vec<&str> = number_tokens[1].split_whitespace().collect();

    let mut card = ScratchCard::new(card_num);
    for number_str in winning_number_tokens {
        card.winning_numbers.push(number_str.parse::<i32>().unwrap());
    }
    for number_str in card_number_tokens {
        card.card_numbers.push(number_str.parse::<i32>().unwrap());
    }

    card
}

pub fn read_cards(reader: &mut dyn BufRead) -> Vec<ScratchCard> {
    let mut cards: Vec<ScratchCard> = Vec::new();

    for line_result in reader.lines() {
        let line = line_result.unwrap();
        let line_trimmed = line.trim();

        cards.push(read_card_line(line_trimmed));
    }

    cards
}

pub fn winning_number_count(card: &ScratchCard) -> u32 {
    let mut count = 0;
    for num in &card.card_numbers {
        if card.winning_numbers.contains(num) {
            count += 1;
        }
    }
    count
}

pub fn card_score(card: &ScratchCard) -> u32 {
    let c = winning_number_count(card);
    if c > 0 {
        u32::pow(2, c - 1)
    } else {
        0
    }
}

pub fn card_pile_score(cards: &Vec<ScratchCard>) -> u32 {
    let mut s = 0;

    for card in cards {
        s += card_score(card);
    }

    s
}

pub fn total_scratchcard_count(cards: &[ScratchCard]) -> u32 {
    let mut card_stack = cards.to_owned();

    let mut total = 0;
    while !card_stack.is_empty() {
        total += 1;
        let card = card_stack.pop().unwrap();
        let w = winning_number_count(&card);
        for i in card.card_num..card.card_num+w {
            card_stack.push(cards[i as usize].clone());
        }
    }

    total
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read_card_line_test() {
        let card = read_card_line("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53");
        assert_eq!(1, card.card_num);
        assert_eq!(5, card.winning_numbers.len());
        assert_eq!(8, card.card_numbers.len());
    }

    #[test]
    fn winning_number_count_test() {
        let card = read_card_line("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53");
        assert_eq!(4, winning_number_count(&card));
    }

    #[test]
    fn card_score_test() {
        let card = read_card_line("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53");
        assert_eq!(8, card_score(&card));
    }
}