use advent_of_code_2023::day1::*;

use std::io::BufReader;
use std::fs::File;
use std::path::Path;

#[test]
fn day1_part1_example() {
    let input_path = Path::new("data/day1_part1_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let cal_vec = read_cal_vector(&mut reader, false);
        assert_eq!(4, cal_vec.len());
        assert_eq!(12, cal_vec[0]);
        assert_eq!(38, cal_vec[1]);
        assert_eq!(15, cal_vec[2]);
        assert_eq!(77, cal_vec[3]);

        let cal_sum = cal_sum(&cal_vec);
        assert_eq!(142, cal_sum);
    }
}

#[test]
fn day1_part1_actual() {
    let input_path = Path::new("data/day1_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let cal_vec = read_cal_vector(&mut reader, false);
        let cal_sum = cal_sum(&cal_vec);
        assert_eq!(55002, cal_sum);
    }
}

#[test]
fn day1_part2_example() {
    let input_path = Path::new("data/day1_part2_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let cal_vec = read_cal_vector(&mut reader, true);
        assert_eq!(7, cal_vec.len());
        assert_eq!(29, cal_vec[0]);
        assert_eq!(83, cal_vec[1]);
        assert_eq!(13, cal_vec[2]);
        assert_eq!(24, cal_vec[3]);
        assert_eq!(42, cal_vec[4]);
        assert_eq!(14, cal_vec[5]);
        assert_eq!(76, cal_vec[6]);

        let cal_sum = cal_sum(&cal_vec);
        assert_eq!(281, cal_sum);
    }
}
#[test]
fn day1_part2_actual() {
    let input_path = Path::new("data/day1_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let cal_vec = read_cal_vector(&mut reader, true);
        let cal_sum = cal_sum(&cal_vec);
        assert_eq!(55093, cal_sum);
    }
}