use advent_of_code_2023::day6::*;

use std::io::BufReader;
use std::fs::File;
use std::path::Path;

#[test]
fn day6_part1_example() {
    let input_path = Path::new("data/day6_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let races = read_races(&mut reader);
        assert_eq!(3, races.len());
        assert_eq!(288, winning_duration_product(&races));
    }
}

#[test]
fn day6_part1_actual() {
    let input_path = Path::new("data/day6_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let races = read_races(&mut reader);
        assert_eq!(281600, winning_duration_product(&races));
    }
}

#[test]
fn day6_part2_example() {
    let input_path = Path::new("data/day6_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let races = read_race(&mut reader);
        assert_eq!(1, races.len());
        assert_eq!(71503, winning_duration_product(&races));
    }
}

#[test]
fn day6_part2_actual() {
    let input_path = Path::new("data/day6_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let races = read_race(&mut reader);
        assert_eq!(33875953, winning_duration_product(&races));
    }
}