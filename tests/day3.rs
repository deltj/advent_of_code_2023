use advent_of_code_2023::day3::*;

use std::io::BufReader;
use std::fs::File;
use std::path::Path;

#[test]
fn day3_part1_example() {
    let input_path = Path::new("data/day3_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let schematic = read_engine_schematic(&mut reader);
        assert_eq!(10, schematic.numbers.len());
        assert_eq!(6, schematic.symbols.len());
        assert_eq!(4361, part_number_sum(&schematic));
    }
}

#[test]
fn day3_part1_actual() {
    let input_path = Path::new("data/day3_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let schematic = read_engine_schematic(&mut reader);
        assert_eq!(527144, part_number_sum(&schematic));
    }
}

#[test]
fn day3_part2_example() {
    let input_path = Path::new("data/day3_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let schematic = read_engine_schematic(&mut reader);
        assert_eq!(467835, gear_ratio_sum(&schematic));
    }
}

#[test]
fn day3_part2_actual() {
    let input_path = Path::new("data/day3_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let schematic = read_engine_schematic(&mut reader);
        assert_eq!(81463996, gear_ratio_sum(&schematic));
    }
}