use advent_of_code_2023::day5::*;

use std::io::BufReader;
use std::fs::File;
use std::path::Path;

#[test]
fn day5_part1_example() {
    let input_path = Path::new("data/day5_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let (seeds, maps) = read_maps(&mut reader, false);
        assert_eq!(4, seeds.len());
        assert_eq!(35, find_lowest_loc_bf(&seeds, &maps));
    }
}

#[test]
fn day5_part1_actual() {
    let input_path = Path::new("data/day5_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let (seeds, _maps) = read_maps(&mut reader, false);
        assert_eq!(20, seeds.len());
        //assert_eq!(510109797, find_lowest_loc_bf(&seeds, &maps));
    }
}

#[test]
fn day5_part2_example() {
    let input_path = Path::new("data/day5_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let (seeds, maps) = read_maps(&mut reader, true);
        assert_eq!(2, seeds.len());
        assert_eq!(46, find_lowest_loc_bf(&seeds, &maps));
    }
}

#[test]
fn day5_part2_actual() {
    let input_path = Path::new("data/day5_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let (seeds, _maps) = read_maps(&mut reader, true);
        assert_eq!(10, seeds.len());
        //assert_eq!(0, find_lowest_loc_bf(&seeds, &maps));
    }
}
