use advent_of_code_2023::day4::*;

use std::io::BufReader;
use std::fs::File;
use std::path::Path;

#[test]
fn day4_part1_example() {
    let input_path = Path::new("data/day4_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let cards = read_cards(&mut reader);
        let score = card_pile_score(&cards);
        assert_eq!(13, score);
    }
}

#[test]
fn day4_part1_actual() {
    let input_path = Path::new("data/day4_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let cards = read_cards(&mut reader);
        let score = card_pile_score(&cards);
        assert_eq!(19855, score);
    }
}

#[test]
fn day4_part2_example() {
    let input_path = Path::new("data/day4_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let cards = read_cards(&mut reader);
        let count = total_scratchcard_count(&cards);
        assert_eq!(30, count);
    }
}

#[test]
fn day4_part2_actual() {
    //  This takes a while to run
    /*
    let input_path = Path::new("data/day4_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let cards = read_cards(&mut reader);
        let count = total_scratchcard_count(&cards);
        assert_eq!(0, count);
    }
    */
}