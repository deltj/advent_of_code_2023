use advent_of_code_2023::day7::*;

use std::io::BufReader;
use std::fs::File;
use std::path::Path;
use serial_test::serial;

#[test]
#[serial]
fn day7_part1_example() {
    let input_path = Path::new("data/day7_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        use_jokers(false);
        let hands = read_hands(&mut reader);
        let total_winnings = total_winnings(&hands);
        assert_eq!(6440, total_winnings);
    }
}

#[test]
#[serial]
fn day7_part1_actual() {
    let input_path = Path::new("data/day7_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        use_jokers(false);
        let hands = read_hands(&mut reader);
        let total_winnings = total_winnings(&hands);
        assert_eq!(253954294, total_winnings);
    }
}

#[test]
#[serial]
fn day7_part2_example() {
    let input_path = Path::new("data/day7_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        use_jokers(true);
        let hands = read_hands(&mut reader);
        let total_winnings = total_winnings(&hands);
        assert_eq!(5905, total_winnings);
    }
}

#[test]
#[serial]
fn day7_part2_actual() {
    let input_path = Path::new("data/day7_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        use_jokers(true);
        let hands = read_hands(&mut reader);
        let total_winnings = total_winnings(&hands);
        assert_eq!(254837398, total_winnings);
    }
}