use advent_of_code_2023::day2::*;

use std::io::BufReader;
use std::fs::File;
use std::path::Path;

#[test]
fn day2_part1_example() {
    let input_path = Path::new("data/day2_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let game_vec = read_game_vector(&mut reader);
        assert_eq!(5, game_vec.len());

        let max_red = 12;
        let max_green = 13;
        let max_blue = 14;
        let valid_game_id_sum = sum_of_valid_game_ids(&game_vec, max_red, max_green, max_blue);
        assert_eq!(8, valid_game_id_sum);
    }
}

#[test]
fn day2_part1_actual() {
    let input_path = Path::new("data/day2_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let game_vec = read_game_vector(&mut reader);

        let max_red = 12;
        let max_green = 13;
        let max_blue = 14;
        let valid_game_id_sum = sum_of_valid_game_ids(&game_vec, max_red, max_green, max_blue);
        assert_eq!(2795, valid_game_id_sum);
    }
}

#[test]
fn day2_part2_example() {
    let input_path = Path::new("data/day2_example.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let game_vec = read_game_vector(&mut reader);
        let total_cube_power = total_cube_power(&game_vec);
        assert_eq!(2286, total_cube_power);
    }
}

#[test]
fn day2_part2_actual() {
    let input_path = Path::new("data/day2_actual.txt");
    if input_path.exists() {
        let f = File::open(input_path).unwrap();
        let mut reader = BufReader::new(f);
        let game_vec = read_game_vector(&mut reader);
        let total_cube_power = total_cube_power(&game_vec);
        assert_eq!(75561, total_cube_power);
    }
}
