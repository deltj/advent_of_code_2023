# Advent of Code 2023

This repo includes my solutions for [AoC 2023](https://adventofcode.com/2023).  I decided to use [Rust](https://www.rust-lang.org/) again this year because I like the language and I believe solving problems like this is a good way to become more proficient in a language.

I used the latest version of the rust for this project, which as of this writing is 1.74.0.

## Structure

This is a library crate with one module for each day of the AoC.  Unit tests for functions in the day modules are collocated in the module in the `src` dir, and integration tests that read puzzle input from a file are in the `tests` dir.